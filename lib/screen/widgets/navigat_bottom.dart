import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:project1/constants/appAssets.dart';
import 'package:project1/constants/appColors.dart';
import 'package:project1/generated/l10n.dart';

import '../persons_screen.dart';
import '../settings_screen.dart';

class NavigationBottom extends StatelessWidget {
  const NavigationBottom({
    Key? key,
    required this.counter,
  }) : super(key: key);

  final int counter;

  PageRouteBuilder _fadeRoute(Widget screen) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => screen,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return child;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: counter,
      unselectedItemColor: AppColors.neutral2,
      selectedItemColor: AppColors.primary,
      selectedFontSize: 15.0,
      unselectedFontSize: 15.0,
      items: [
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.subtract,
          ),
          activeIcon: SvgPicture.asset(
            AppAssets.svg.subtract,
            color: AppColors.primary,
          ),
          label: S.of(context).person,
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.settings_outlined),
          label: S.of(context).setting,
        ),
      ],
      onTap: (index) {
        if (index == 0) {
          Navigator.of(context).pushAndRemoveUntil(
            _fadeRoute(const PersonScreen()),
            (route) => false,
          );
        } else if (index == 1) {
          Navigator.of(context).pushAndRemoveUntil(
            _fadeRoute(const SettingScreen()),
            (route) => false,
          );
        }
      },
    );
  }
}
