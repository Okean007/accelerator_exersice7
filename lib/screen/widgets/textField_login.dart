// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../constants/appAssets.dart';
import '../../constants/appColors.dart';
import '../../generated/l10n.dart';

class LoginTextField extends StatelessWidget {
  const LoginTextField({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SvgPicture.asset(
            AppAssets.svg.login,
            width: 16.0,
            color: AppColors.neutral2,
          ),
        ),
        hintText: S.of(context).login,
        border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            borderSide: BorderSide.none),
        fillColor: AppColors.neutral1,
        filled: true,
        counterText: '',
      ),
      maxLength: 8,
      controller: controller,
      validator: (login) {
        if (login == null || login.length < 3) {
          return S.of(context).inputErrorLoginIsShort;
        }
        return null;
      },
    );
  }
}
