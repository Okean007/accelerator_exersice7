import 'package:flutter/material.dart';
import 'package:project1/constants/appColors.dart';
import 'package:project1/constants/appStyles.dart';
import 'package:project1/generated/l10n.dart';
import 'package:project1/screen/widgets/grid_view.dart';
import 'package:project1/screen/widgets/list_view.dart';
import 'package:project1/screen/widgets/search_widget.dart';
import 'package:project1/screen/widgets/vmodel.dart';
import 'package:provider/provider.dart';

import '../repo/repo_person.dart';
import 'widgets/navigat_bottom.dart';

class PersonScreen extends StatelessWidget {
  const PersonScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //List<Person> personList = getPersonList();
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: const NavigationBottom(counter: 0),
        body: ChangeNotifierProvider(
          create: (context) => PersonsListVModel(
            repo: Provider.of<RepoPersons>(context, listen: false),
          ),
          builder: (context, _) {
            final personsTotal =
                context.watch<PersonsListVModel>().filteredList.length;
            return Column(
              children: [
                SearchField(
                  onChanged: (value) {
                    Provider.of<PersonsListVModel>(context, listen: false)
                        .filter(
                      value.toLowerCase(),
                    );
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          S
                              .of(context)
                              .personsTotal(personsTotal)
                              .toUpperCase(),
                          style: AppStyles.s10w500.copyWith(
                            letterSpacing: 1.5,
                            color: AppColors.neutral2,
                          ),
                        ),
                      ),
                      IconButton(
                        icon: const Icon(Icons.grid_view),
                        iconSize: 28.0,
                        color: AppColors.neutral2,
                        onPressed: () {
                          Provider.of<PersonsListVModel>(
                            context,
                            listen: false,
                          ).switchView();
                        },
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Consumer<PersonsListVModel>(
                    builder: (context, vmodel, _) {
                      if (vmodel.isLoading) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            CircularProgressIndicator(),
                          ],
                        );
                      }
                      if (vmodel.errorMessage != null) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(vmodel.errorMessage!),
                            ),
                          ],
                        );
                      }
                      if (vmodel.filteredList.isEmpty) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(S.of(context).personsListIsEmpty),
                            ),
                          ],
                        );
                      }
                      return vmodel.isListView
                          ? PersonListView(
                              personsList: vmodel.filteredList,
                            )
                          : PersonGridView(
                              personsList: vmodel.filteredList,
                            );
                    },
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
