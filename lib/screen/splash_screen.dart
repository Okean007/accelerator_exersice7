import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project1/constants/appAssets.dart';
import 'package:project1/login_screen.dart';
import 'package:project1/repo/repo_settings.dart';
import 'package:provider/provider.dart';

import '../generated/l10n.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void dispose() {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: Colors.white70,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
    );
    super.dispose();
  }

  @override
  void initState() {
    Future.delayed(const Duration(seconds: 2)).whenComplete(() {
      final repoSettings = Provider.of<RepoSettings>(
        context,
        listen: false,
      );
      repoSettings.init().whenComplete(() async {
        var defaultLocale = const Locale('ru', 'RU');
        final locale = await repoSettings.readLocale();
        if (locale == 'en') {
          defaultLocale = const Locale('en');
        }
        S.load(defaultLocale).whenComplete(() {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (context) => const LoginScreen(),
            ),
          );
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Stack(
        children: [
          Positioned.fill(
            child: Image.asset(
              AppAssets.images.background,
            ),
          ),
          Positioned(
            top: 10,
            left: 40,
            child: Column(
              children: [
                SizedBox(
                  child: Image.asset(
                    AppAssets.images.logotip,
                  ),
                ),
                SizedBox(
                  child: Image.asset(
                    AppAssets.images.morty,
                  ),
                ),
                SizedBox(
                  child: Image.asset(
                    AppAssets.images.rick,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
