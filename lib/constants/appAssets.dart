// ignore_for_file: file_names

abstract class AppAssets {
  static const images = _Images();
  static const svg = _Svg();
}

class _Images {
  const _Images();
  final String background = 'assets/images/bitmap/backgroung.png';
  final String noAvatar = 'assets/images/bitmap/no_avatar.png';
  final String logotip = 'assets/images/bitmap/logotip.png';
  final String rick = 'assets/images/bitmap/rick.png';
  final String morty = 'assets/images/bitmap/morty.png';
}

class _Svg {
  const _Svg();
  final String login = 'assets/images/svg/group.svg';
  final String password = 'assets/images/svg/password.svg';
  final String subtract = 'assets/images/svg/subtract.svg';
}
